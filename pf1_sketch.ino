#include <Arduino.h>

const int pins[] = {11, 13}; //11 is PWM and 13 is dirB
const int brakePin = 8;
const int leftSensorPin = A0; // analog pins with sensors
const int rightSensorPin = A1;
int sensorThreshold = 30; // must have this much light on a sensor to move
int mySpeed;
bool fullStop;
bool left;
bool right;

void setup() {
    pinMode(pins[1], OUTPUT);
    pinMode(brakePin, OUTPUT);
    Serial.begin(9600);
    bool fullStop = false;
    bool left = false;
    bool right = false;
}

void loop() {
    Serial.println(mySpeed);
    int leftVal = analogRead(leftSensorPin);
    int rightVal = analogRead(rightSensorPin);
    if (leftVal - sensorThreshold > rightVal) {
        if (!1eft) {
            fullStop = false;
            right = false;
            left = true;

            changeDirection(pins);
        }

        changeSpeed(pins);
    } else if (rightVal - sensorThreshold > leftVal) {
        if (!right) {
            fullStop = false;
            left = false;
            right = true;

            changeDirection(pins);
        }
        changeSpeed(pins);
    } else {
        left = false;
        right = false;
        fullStop = true;

        changeDirection(pins);
    }
}

void changeSpeed(int pins[]) {
    if (mySpeed < 250) {
        mySpeed += 5;
        delay(50);
    }
    analogWrite(pins[0], constrain(mySpeed, 50, 255));
}

void changeDirection(int pins[]) {
    if (left) //left
    {
        while (mySpeed > 0) {
            mySpeed -= 5;
        }
        digitalWrite(pins[1], LOW);
        digitalWrite(brakePin, LOW);

    }
    if (right) //right
    {
        while (mySpeed > 0) {
            mySpeed -= 5;
        }
        digitalWrite(pins[1], HIGH);
        digitalWrite(brakePin, LOW);

    }
    if (fullStop) //fullstop
    {
        digitalWrite(brakePin, HIGH);
        while (mySpeed > 0) {
            mySpeed -= 5;
        }
        analogWrite(pins[0], constrain(mySpeed, 0, 255));
    }
}


